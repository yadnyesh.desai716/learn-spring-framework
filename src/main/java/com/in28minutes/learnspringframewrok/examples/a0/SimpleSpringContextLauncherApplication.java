package com.in28minutes.learnspringframewrok.examples.a0;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Stream;

@Configuration
//  Spring will automatically perform the ComponentScan for the current package, so
//  no need to explicitly mention the package name.
@ComponentScan
public class SimpleSpringContextLauncherApplication {

    public static void main(String[] args) {

        var context = new AnnotationConfigApplicationContext(SimpleSpringContextLauncherApplication.class);
        System.out.println(context.getBeanDefinitionCount());
        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
