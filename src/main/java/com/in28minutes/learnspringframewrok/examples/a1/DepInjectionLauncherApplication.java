package com.in28minutes.learnspringframewrok.examples.a1;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
class YourBusinessClass {
    @Autowired  //  Field Dependency Injection
    Dependency1 dependency1;
    @Autowired  //  Field Dependency Injection
    Dependency2 dependency2;

    @Autowired  //  Constructor Dependency Injection
    public YourBusinessClass(Dependency1 dependency1, Dependency2 dependency2) {
        this.dependency1 = dependency1;
        this.dependency2 = dependency2;
        System.out.println("Constructor Dependency Injection");
    }

    @Autowired  //  Setter Dependency Injection
    public void setDependency1(Dependency1 dependency1) {
        this.dependency1 = dependency1;
        /*  Check whether setter is being used in Dependency Injection by printing
           something to the console. */
        System.out.println("setter Dependency1");
    }

    @Autowired  //  Setter Dependency Injection
    public void setDependency2(Dependency2 dependency2) {
        this.dependency2 = dependency2;
        System.out.println("setter Dependency2");
    }

    @PostConstruct
    public void postConstruct() {
        dependency1.getReady();
    }

    @PreDestroy
    public void preDestroy() {
        dependency1.cleanUp();
    }

    public String toString() {
        return "Using " + dependency1 + " and " + dependency2;
    }
}

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON) //  Bean's scope is Singleton. Only one object instance per Spring IoC Container.
@Lazy    // Lazy initialization
class Dependency1 {

    public void getReady() {
        System.out.println("getReady() is called immediately after Dependency Injection(DI) is completed. This" +
                " can be achieved using @PostConstruct annotation.");
    }

    public void cleanUp() {
        System.out.println("cleanUp() is called.");
    }
}

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE) //  Bean's scope is Prototype. Possibly multiple object instances per Spring IoC Container.
@Lazy(value = false)    //  i.e. Eager initialization
class Dependency2 {
}

@Configuration
/*
  Spring will automatically perform the ComponentScan for the current package, so
  no need to explicitly mention the package name.
*/
@ComponentScan
public class DepInjectionLauncherApplication {

    public static void main(String[] args) {

        var context = new AnnotationConfigApplicationContext(DepInjectionLauncherApplication.class);
        System.out.println(context.getBeanDefinitionCount());
        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);
        System.out.println(context.getBean(YourBusinessClass.class));

        /*
          Notice the hash codes in the console.
          Singleton Bean's hash code is uniques, means there is only one instance present.
          Prototype Bean's hash code is different, means each time a new instance is created.
        */
        System.out.println("Singleton Scope: " + context.getBean(Dependency1.class));
        System.out.println("Singleton Scope: " + context.getBean(Dependency1.class));
        System.out.println("Singleton Scope: " + context.getBean(Dependency1.class));
        System.out.println();
        System.out.println("Prototype Scope: " + context.getBean(Dependency2.class));
        System.out.println("Prototype Scope: " + context.getBean(Dependency2.class));
        System.out.println("Prototype Scope: " + context.getBean(Dependency2.class));
    }
}
