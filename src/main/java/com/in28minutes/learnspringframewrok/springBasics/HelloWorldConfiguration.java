package com.in28minutes.learnspringframewrok.springBasics;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

record Person(String name, int age, Address address) {
}

record Address(String city, String state) {

}

@Configuration  //  @Configuration annotation indicates that this is a Spring Configuration class.
public class HelloWorldConfiguration {
    @Bean(name = "FirstN")
    public String firstName() {
        return "Yadnyesh";
    }

    @Bean
    public int age() {
        return 30;
    }

    @Bean
    public Person person() {
        return new Person("Adam", 33, new Address("Manhattan", "New York"));
    }

    @Bean(name = "Person Method Call")
    public Person person2MethodCall() {
        //  Retrieving beans by using firstName() & age() method calls.
        return new Person(firstName(), age(), addressMP());
    }

    @Bean(name = "Person Parameters")
    public Person person3Parameters(String name, int age, Address address) {
        //  Retrieving beans by using Parameters.
        return new Person(name, age, address);
    }

    @Bean(name = "Person Qualifier Parameters")
    public Person person4Parameters(String name, int age, @Qualifier("MP") Address address) {
        //  Retrieving beans by using Parameters.
        return new Person(name, age, address);
    }

    @Bean(name = "MH Address")
    @Primary
    public Address address() {
        return new Address("Pune", "MH");
    }

    @Bean(name = "MP Address")
    @Qualifier("MP")
    public Address addressMP() {
        return new Address("Indore", "MP");
    }
}
