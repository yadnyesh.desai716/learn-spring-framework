package com.in28minutes.learnspringframewrok.game;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GameRunner {
    private final GamingConsole game;

    //  GamingConsole interface makes GameRunner class loosely-coupled with game classes.
    //  Auto-wiring is happening through "Constructor-based Dependency Injection".
    public GameRunner(@Qualifier("superContraGame") GamingConsole game) {
        this.game = game;
    }

    public void run() {
        System.out.println("Running game: " + game);
        game.up();
        game.down();
        game.left();
        game.right();
    }
}
