package com.in28minutes.learnspringframewrok;

import com.in28minutes.learnspringframewrok.game.*;

public class App01GamingBasicJava {

    public static void main(String[] args) {

        //  Object creation.
        GamingConsole marioGame = new MarioGame();  //  Follow "Coding to interface" style of coding.
        GamingConsole superContraGame = new SuperContraGame();
        GamingConsole pacManGame = new PacManGame();

        //  Wiring of dependencies.
        //  GamingConsole is the dependency of GameRunner class.
        var gameRunner = new GameRunner(marioGame);
        gameRunner.run();
    }
}
